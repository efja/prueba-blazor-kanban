# Ejercicio: Tablero Kanban con Blazor Server

## Descripción

Desarrolla una aplicación de tablero Kanban simple utilizando Blazor Server. La aplicación debe permitir a los usuarios crear, editar y eliminar tareas, así como moverlas entre columnas.

## Requisitos

1. Página Principal (Index):
    - Mostrar tres columnas: "Por Hacer", "En Proceso" y "Hecho".
    - Mostrar todas las tareas en su columna correspondiente.
    - Cada tarea debe tener un título, una descripción y un botón para eliminarla.

2. Creación de Tareas:
    - Agregar un botón "Agregar Tarea" que abre un formulario modal.
    - El formulario debe tener campos para el título y la descripción de la tarea.
    - Al agregar una tarea, esta debe aparecer en la columna "Por Hacer".

3. Edición de Tareas:
    - Al hacer clic en una tarea, permitir la edición del título y la descripción en un formulario modal.
    - Al guardar los cambios, la tarea debe actualizarse en el tablero.

4. Movimiento entre Columnas:
    - Permitir arrastrar y soltar tareas entre las columnas.
    - Al mover una tarea a una nueva columna, actualizar su estado.

5. Persistencia de Datos:
    - Al recargar la página, las tareas deben persistir.

> **Notas:** no utilices ningún framework. No utilices los estilos de ejemplo de Visual Studio u otro IDE, puedes usar Bootstrap pero crea tu propio layout.
